import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import Form from "react-bootstrap/Form";

class SignIn extends React.Component {
    state = {};

    handleInputChange = (event, value) => {
        const target = event.target;
        value ??= target.type === "checkbox" ? target.checked : target.value;
        this.setState({ [target.name]: value });
    };

    onSubmitHandler = ev => {
        ev.preventDefault();

        fetch("https://localhost:7032/users/login", {
            method: "POST",
            headers: new Headers({ "Content-Type": "application/json" }),
            body: JSON.stringify(this.state),
        })
            .then(res => res.json())
            .then(res => {
                if (res?.status === 500) alert(res.detail);
                else this.props.onNewToken(res.token);
            });
    };

    render() {
        return (
            <div>
                <Form className="my-4" autoComplete="on" onSubmit={this.onSubmitHandler}>
                    <Form.FloatingLabel label="Username" controlId="username-label">
                        <Form.Control
                            name="username"
                            type="text"
                            // pattern="[\w\d]+"
                            required
                            onChange={this.handleInputChange}
                        />
                    </Form.FloatingLabel>
                    <Form.FloatingLabel label="Password">
                        <Form.Control
                            name="password"
                            type="password"
                            // pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[\w\d]{8,}"
                            required
                            onChange={this.handleInputChange}
                        />
                    </Form.FloatingLabel>
                    <Form.Control type="submit" value="Sign In" />
                </Form>
            </div>
        );
    }
}

export default SignIn;
