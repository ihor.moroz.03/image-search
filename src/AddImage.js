import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import Form from "react-bootstrap/Form";

class AddImage extends React.Component {
    state = {};

    handleInputChange = (event, value) => {
        const target = event.target;
        value ??= target.type === "checkbox" ? target.checked : target.value;
        this.setState({ [target.name]: value });
    };

    onSubmitHandler = ev => {
        ev.preventDefault();

        fetch("https://localhost:7032/images", {
            method: "POST",
            headers: new Headers({ "Content-Type": "application/json" }),
            body: JSON.stringify({ token: this.props.token, image: this.state }),
        });
    };

    render() {
        return (
            <div>
                <Form className="my-4" autoComplete="on" onSubmit={this.onSubmitHandler}>
                    <Form.FloatingLabel label="Title" controlId="title-label">
                        <Form.Control
                            name="Title"
                            type="text"
                            required
                            onChange={this.handleInputChange}
                        />
                    </Form.FloatingLabel>
                    <Form.FloatingLabel label="URI">
                        <Form.Control
                            name="URI"
                            type="text"
                            required
                            onChange={this.handleInputChange}
                        />
                    </Form.FloatingLabel>
                    <Form.Control type="submit" value="Add" />
                </Form>
            </div>
        );
    }
}

export default AddImage;
