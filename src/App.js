import React from "react";
import "./App.css";
import SignUp from "./SignUp";
import SignIn from "./SignIn";
import AddImage from "./AddImage";

import "bootstrap/dist/css/bootstrap.min.css";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Card from "react-bootstrap/Card";

class App extends React.Component {
    state = { images: [] };

    onNewTokenHandler = newToken => {
        this.setState({ token: newToken });

        if (this.state.interval) clearInterval(this.state.interval);
        const interval = setInterval(() => {
            fetch(`https://localhost:7032/users/${newToken}`)
                .then(res => res.json())
                .then(res => {
                    if (res?.status === 500) alert(res.detail);
                    else this.setState({ status: res });
                });
        }, 500);

        this.setState({ interval: interval });
    };

    searchQuery = query => {
        if (query === "") {
            this.setState({ images: [] });
            return;
        }
        fetch(`https://localhost:7032/images/query/${query}`)
            .then(res => res.json())
            .then(res => {
                if (res?.status === 500) alert(res.detail);
                else this.setState({ images: res });
            });
    };

    render() {
        const status = this.state.status;
        return (
            <div className="App">
                <h2>
                    {status?.signedIn
                        ? `Hello, ${status.username}, you are ${
                              status.isAdmin ? "admin" : "not admin"
                          }`
                        : "Not signed in"}
                </h2>
                <Tabs defaultActiveKey="signin" id="signTabs" className="mb-3">
                    <Tab eventKey="signin" title="Sign In">
                        <SignIn onNewToken={this.onNewTokenHandler} />
                    </Tab>
                    <Tab eventKey="signup" title="Sign Up">
                        <SignUp onNewToken={this.onNewTokenHandler} />
                    </Tab>
                </Tabs>

                <Tabs defaultActiveKey="search" id="imageTabs" className="mb-3">
                    <Tab eventKey="search" title="Search">
                        <input
                            type="text"
                            onChange={ev => {
                                this.searchQuery(ev.target.value);
                            }}
                        ></input>
                        <div style={{ display: "flex", flexWrap: "wrap" }}>
                            {this.state?.images.map((img, i) => (
                                <Card key={"img" + i} style={{ width: "18rem", maxWidth: "50%" }}>
                                    <Card.Img variant="top" src={img.uri} />
                                    <Card.Body>
                                        <Card.Title>{img.title}</Card.Title>
                                        <Card.Text>{`Added by ${img.user.username}`}</Card.Text>
                                        {/* <Button variant="primary">Go somewhere</Button> */}
                                    </Card.Body>
                                </Card>
                            ))}
                        </div>
                    </Tab>
                    {status?.isAdmin ? (
                        <Tab eventKey="add" title="Add">
                            <AddImage token={this.state.token} />
                        </Tab>
                    ) : (
                        <></>
                    )}
                </Tabs>
            </div>
        );
    }
}

export default App;
